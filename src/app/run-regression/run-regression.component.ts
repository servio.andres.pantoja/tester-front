import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-run-regression',
  templateUrl: './run-regression.component.html',
  styleUrls: ['./run-regression.component.css']
})
export class RunRegressionComponent implements OnInit {

  reports = [];
  fileVersionOne = undefined;
  fileVersionTwo = undefined;
  ifProcessed: Boolean = false;
  processing: Boolean = false;
  processedOne: Boolean = false;
  processedTwo: Boolean = false;
  generatingData: Boolean = false;
  versionOne: string;
  versionTwo: string;
  countScreenshots = 0;


  constructor(private http: HttpClient) {

  }

  versionOneSelected(event) {
    console.log(event);
    this.fileVersionOne = <File>event.target.files[0];
  }

  versionTwoSelected(event) {
    console.log(event);
    this.fileVersionTwo = <File>event.target.files[0];
  }

  async startRegression() {
    this.processing = true;
    this.processedOne = false;
    this.processedTwo = false;
    this.ifProcessed = false;
    this.generatingData = false;

    let executionId = new Date().getMilliseconds().toString();
    let fileName = new Date().getMilliseconds().toString() + ".feature";
    const formDataOne = new FormData();
    console.log(`Archivo uno: ${this.fileVersionOne.name}`);
    formDataOne.append('file', this.fileVersionOne, fileName);
    formDataOne.append('executionId', executionId);
    const urlOne = 'http://localhost:3000/regression/one';
    let responseExecutionOne = await this.http.post(urlOne, formDataOne).toPromise();
    await this.delay(2000);
    this.processedOne = true;
    await this.delay(2000);
    const formDataTwo = new FormData();
    console.log(`Archivo dos: ${this.fileVersionTwo.name}`);
    formDataTwo.append('file', this.fileVersionTwo, fileName);
    formDataTwo.append('executionId', executionId);
    const urlTwo = 'http://localhost:3000/regression/two';
    let responseExecutionTwo = await this.http.post(urlTwo, formDataTwo).toPromise();
    this.processedTwo = true;
    await this.delay(2000);
    this.generatingData = true;
    this.detectVersions(this.fileVersionOne, true);
    await this.delay(1000);
    this.detectVersions(this.fileVersionTwo, false);
    await this.delay(1000);
    this.loadResources();
    await this.delay(2000);
    this.generatingData = false;
    this.ifProcessed = true;
    this.processing = false;



  }

  public async showReportVersion() {
    this.detectVersions(this.fileVersionOne, true);
    await this.delay(1000);
    this.detectVersions(this.fileVersionTwo, false);
    await this.delay(1000);
    this.ifProcessed = true;
    this.processing = false;
  }

  public detectVersions(file, versionOne: boolean) {
    this.countScreenshots = 0;
    let reader = new FileReader();
    reader.onload = () => {
      var text = reader.result;
      console.log(text);
      var rows = text.toString().split("\n");
      rows.forEach(row => {
        if (row.includes("I define a version")) {
          console.log("Version found");
          let rowSplited = row.split("\"");
          console.log("version", rowSplited[1]);
          if (versionOne) {
            this.versionOne = rowSplited[1];
          } else {
            this.versionTwo = rowSplited[1];
          }
        }

        if (row.includes("I take a screenshot")) {
          this.countScreenshots = this.countScreenshots + 1;
        }

      });
    }
    reader.readAsText(file);
  }

  public getCounts() {
    var arr_names: number[] = [];
    for (var i = 0; i < this.countScreenshots; i++) {
      arr_names.push(i);
      console.log(arr_names[i])
    }
    return arr_names;
  }

  // async generateReport() {
  //   let report = await this.http.get("http://ec2-54-233-109-157.sa-east-1.compute.amazonaws.com:3000/regression").toPromise();
  //   let information = await this.http.get("http://ec2-54-233-109-157.sa-east-1.compute.amazonaws.com:3000/reports/" + report["id"] + "/information").toPromise();
  //   report["information"] = information;
  //   this.reports.push(report);
  // }
  ngOnInit() {

  }

  async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async loadResources() {
    for (var i = 0; i < this.countScreenshots; i++) {
      let report = await this.http.get("http://localhost:3000/regressions/" + this.versionTwo + "/" + this.versionOne + "/" + i).toPromise();
      if (report) {
        this.reports.push(report);
      }
    }
  }

}
