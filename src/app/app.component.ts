import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  reports = [];

  constructor(private http: HttpClient) {

  }

  async generateReport() {
    let report = await this.http.get('http://ec2-54-233-109-157.sa-east-1.compute.amazonaws.com:3000/regression').toPromise();
    const information = await this.http.get(`http://ec2-54-233-109-157.sa-east-1.compute.amazonaws.com:3000/reports/
                                            ${report['id']}/information`).toPromise();
    report['information'] = information;
    this.reports.push(report);
  }

}
