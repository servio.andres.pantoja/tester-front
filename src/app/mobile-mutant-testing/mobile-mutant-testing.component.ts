import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// const fs = require('fs');

@Component({
  selector: 'app-mobile-mutant-testing',
  templateUrl: './mobile-mutant-testing.component.html',
  styleUrls: ['./mobile-mutant-testing.component.css']
})
export class MobileMutantTestingComponent implements OnInit {

  selectedFile: File = null;
  packageName: string;
  seed: string;
  events: string;
  reportId: string;
  testingRun: boolean;
  reportAvailable: boolean;
  htmlReport: string; // string or html ? or file?
  constructor( private http: HttpClient ) {
    this.testingRun = false;
    this.reportAvailable = false;
    this.packageName = '';
    this.seed = '';
    this.events = '';
    this.reportId = '';
  }

  ngOnInit() {
  }

  OnFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {
    if (!this.selectedFile) {
      alert('Debe adjuntar el APK.');
    } else if (this.seed === '' || this.seed === '0') {
      alert('Debe ingresar la semilla.');
    } else if (this.events === '' || this.events === '0') {
      alert('Debe ingresar el número de eventos.');
    } else if (this.packageName === '') {
      alert('Debe ingresar el nombre del package.');
    } else {
      const fd = new FormData();
      console.log(`Archivo: ${this.selectedFile.name}`);
      console.log(`Nombre del paquete: ${this.packageName}`);
      fd.append('file', this.selectedFile, this.selectedFile.name);
      fd.append('package', this.packageName);
      fd.append('events', this.events);
      fd.append('seed', this.seed);
      const url = 'http://localhost:3000/mobile-mutant-testings';
      this.testingRun = true;
      this.http.post(url, fd).subscribe(response => {
        console.log(response);
        if (response) {
          // const obj = JSON.parse(response);
          if (response.hasOwnProperty('id')) {
            this.reportId = response['id'];
            // this.getReport(this.reportId);
            this.getReport('1544937113046.apk');
            // setTimeout(function () {
              // this.testingRun = false;
              // this.reportAvailable = true;
            // }, 20000);
          }
        }
      }, err => {
        console.error(err);
        this.testingRun = false;
      });

    }
  }


  getReport(idreport: string): void {
    const url = 'http://localhost:3000/reports';

    // while (this.testingRun) {
      // console.log('dentro del while');
      // setInterval(function() {
        this.http.get(`${url}/${idreport}/monkey`, {responseType: 'text'}).subscribe(response => {
          console.log('respuesta api reporte', response);
          if (response) {
            console.log('Reporte listo!');

            this.htmlReport = response;
            this.downloadFile(response);
          }
        }, error => {
          this.testingRun = false;
          this.reportAvailable = false;
          console.log(error);
        });
      // }, 10000);
    // }
  }

  downloadFile(data: any) {
    const self = this;
    setTimeout(function() {
      console.log('ingresa downloadFile');
      const blob = new Blob([data], { type: 'application/text' });
      const url = window.URL.createObjectURL(blob);
      self.testingRun = false;
      self.reportAvailable = true;
      window.open(url);
      console.log('Finaliza downloadFile');
    }, 210000);

  }

  downloadReport() {
    const self = this;
    setTimeout(function () {
       self.getReport('1544583435654.apk');
    }, 20000);

  }

}
