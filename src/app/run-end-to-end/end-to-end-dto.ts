export class EndToEndDto {
    headless: boolean;
    browsers: string;
    project: string;
}
