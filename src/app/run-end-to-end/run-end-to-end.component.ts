import { Component, OnInit } from '@angular/core';
import { EndToEndDto } from './end-to-end-dto';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-run-end-to-end',
  templateUrl: './run-end-to-end.component.html',
  styleUrls: ['./run-end-to-end.component.css']
})
export class RunEndToEndComponent implements OnInit {

  firefox: boolean;
  chrome: boolean;
  headless: boolean;
  processing: boolean;

  constructor(private http: HttpClient) {
    this.firefox = false;
    this.chrome = false;
    this.headless = false;
    this.processing = false;
  }

  ngOnInit() {
  }

  async startTest() {
    this.processing = true;
    const endToEnd = new EndToEndDto();
    endToEnd.headless = this.headless;
    endToEnd.browsers = '';
    if (this.firefox) {
      endToEnd.browsers = endToEnd.browsers.concat('firefox');
    }
    if (this.chrome) {
      if (endToEnd.browsers === '') {
        endToEnd.browsers = endToEnd.browsers.concat('chrome');
      } else {
        endToEnd.browsers = endToEnd.browsers.concat('chrome');
      }
    }
    endToEnd.project = 'pymeadmin';
    const result = await this.http.post('http://localhost:3000/end-to-ends', endToEnd).toPromise();
    this.processing = false;
  }

}
