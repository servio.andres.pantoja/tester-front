import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RunRegressionComponent } from './run-regression/run-regression.component';
import { RunEndToEndComponent } from './run-end-to-end/run-end-to-end.component';
import { MobileMutantTestingComponent } from './mobile-mutant-testing/mobile-mutant-testing.component';


const routes: Routes = [
  { path: 'run-end-to-end', component: RunEndToEndComponent },
  { path: 'run-regression', component: RunRegressionComponent },
  { path: 'mobile-mutant-testing', component: MobileMutantTestingComponent },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
