import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RunRegressionComponent } from './run-regression/run-regression.component';
import { RunEndToEndComponent } from './run-end-to-end/run-end-to-end.component';
import { FormsModule } from '@angular/forms';
import { MobileMutantTestingComponent } from './mobile-mutant-testing/mobile-mutant-testing.component';

@NgModule({
  declarations: [
    AppComponent,
    RunRegressionComponent,
    RunEndToEndComponent,
    MobileMutantTestingComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
