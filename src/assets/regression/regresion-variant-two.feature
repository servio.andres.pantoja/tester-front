Feature: Regresion testing for login pyme admin
    I should test pyme admin

    Scenario: Login success to los estudiantes
        Given I define a version "e2"
        Given I go to the website "https://losestudiantes.co"
        And I get element with "class" and value "botonCerrar"
        And I click
        And I pause 1000
        And I get element with "class" and value "botonIngresar"
        And I click
        And I get element with "name" and value "correo" and container with "class" and value "cajaLogIn"
        And I send text "shd_cristo@hotmail.com"
        And I take a screenshot
        And I get element with "name" and value "password" and container with "class" and value "cajaLogIn"
        And I send text "ServioPantoja@1"
        And I take a screenshot
        And I get element with "class" and value "logInButton" and container with "class" and value "cajaLogIn"
        And I click
        Then element with "id" and value "cuenta" should exist
        And I take a screenshot
        And Validate with version "e1"
